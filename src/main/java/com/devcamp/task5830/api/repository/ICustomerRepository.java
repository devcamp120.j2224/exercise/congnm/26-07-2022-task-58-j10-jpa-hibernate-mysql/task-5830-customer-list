package com.devcamp.task5830.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task5830.api.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long> {
    
}
