package com.devcamp.task5830.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5830.api.model.CCustomer;
import com.devcamp.task5830.api.repository.ICustomerRepository;

@CrossOrigin
@RestController
public class CCustomerController {
    @Autowired
    ICustomerRepository pCustomerRepository;

    @GetMapping("/api/customers")
    public ResponseEntity<List<CCustomer>> getAllCustomers(){
        try {
            List<CCustomer> listCustomer = new ArrayList<CCustomer>();
            pCustomerRepository.findAll().forEach(listCustomer::add);
            if (listCustomer.size() == 0) {
                return new ResponseEntity<List<CCustomer>>(listCustomer, HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<List<CCustomer>>(listCustomer, HttpStatus.OK);
            }
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
